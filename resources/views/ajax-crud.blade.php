<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>laravel 6 First Ajax CRUD Application - Tutsmake.com</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 <style>
   .container{
    padding: 0.5%;
   }
</style>
</head>
<body>

<div class="container">
    <h2 style="margin-top: 12px;" class="alert alert-success">laravel 6  Ajax CRUD Application - Mans</h2><br>
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-user">Add User</a>
          <table class="table table-bordered" id="laravel_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Name</th>
                 <th>Email</th>
                 <th>Mobile</th>
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="users-crud">
              @foreach($users as $u_info)
              <tr id="user_id_{{ $u_info->id }}">
                 <td>{{ $u_info->id  }}</td>
                 <td>{{ $u_info->name }}</td>
                 <td>{{ $u_info->email }}</td>
                 <td>
                     @foreach ($u_info->mobiles as $mob)
                    {{$mob->mobile}}<br>
                    @endforeach
                 </td>

                 <td><a href="javascript:void(0)" id="edit-user" data-id="{{ $u_info->id }}" class="btn btn-info">Edit</a></td>
                 <td>
                  <a href="javascript:void(0)" id="delete-user" data-id="{{ $u_info->id }}" class="btn btn-danger delete-user">Delete</a></td>
              </tr>
              @endforeach
           </tbody>
          </table>

       </div>
    </div>
</div>
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" id="userCrudModal"></h4>
          </div>
          <div class="modal-body">
              <div id='errors'></div>
              <form id="userForm" name="userForm" class="form-horizontal">
                 <input type="hidden" name="user_id" id="user_id">
                  <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-12">
                          <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                      </div>
                  </div>

                  <div class="form-group">
                      <label class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-12">
                          <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="" required="">
                      </div>
                  </div>
                  <button type="button" class="btn btn-success" id="btn-add-mob" value="New Mobile">Add Mobile </button>
                  <div class="form-group"  id="mobs"></div>
                  <input type="hidden" id="mcount" name="mcount" value="0">
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="btn-save" value="create">Save changes
              </button>
          </div>
      </div>
    </div>
  </div>
</body>
<script>
            var mobcount=0;

    $(document).ready(function () {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      /*  When user click add user button */
      $('#create-new-user').click(function () {
          $('#btn-save').val("create-user");
          $('#userForm').trigger("reset");
          $('#userCrudModal').html("Add New User");
          $('#ajax-crud-modal').modal('show');
      });

     /* When click edit user */
      $('body').on('click', '#edit-user', function () {
        var user_id = $(this).data('id');
        $.get('ajax-crud/'+user_id +'/edit', function (data) {
debugger
           $('#userCrudModal').html("Edit User");
            $('#btn-save').val("edit-user");
            $('#ajax-crud-modal').modal('show');
            $('#user_id').val(data[0].id);
            $('#name').val(data[0].name);
            $('#email').val(data[0].email);

            var aa="";
            var n=data[1].length;
            for (let index =1; index <= n; index++) {

                 aa+='<div class="form-inline"><label class="col-sm-3 control-label">Mobile '+index+'</label>';
            aa+='<input type="text" class="form-control col-sm-5" id="mob'+index+'" name="mob'+index+'" placeholder="Enter Mobile" value="'+data[1][index-1].mobile+'" required="">';
            aa+='  <a onclick="delmob('+index+')" id="btn-del-mob"  class="btn btn-danger col-sm-4">Delete Mobile</a></div>';
            }
            $("#mcount").val(n);
            $("#mobs").html(aa);

        })
     });
     //delete user login
      $('body').on('click', '.delete-user', function () {
          var user_id = $(this).data("id");
         var r= confirm("Are You sure want to delete !");
            if(!r) return 0;
          $.ajax({
              type: "DELETE",
              url: "{{ url('ajax-crud')}}"+'/'+user_id,
              success: function (data) {
                  $("#user_id_" + user_id).fadeOut(500);
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
      });
    });

    $('body').on('click', '#btn-save', function () {

        var actionType = $('#btn-save').val();
        $('#btn-save').html('Sending..');

        $.ajax({
            data: $('#userForm').serialize(),
            url: "ajax-crud/store",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                debugger
                //var user = '<tr id="user_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + data.email + '</td>';
                //user += '<td><a href="javascript:void(0)" id="edit-user" data-id="' + data.id + '" class="btn btn-info">Edit</a></td>';
                //user += '<td><a href="javascript:void(0)" id="delete-user" data-id="' + data.id + '" class="btn btn-danger delete-user">Delete</a></td></tr>';
                var res=data[0];
                var user=data[1];
                if(res=='suc')
                {
                    if (actionType == "create-user") {
                        $('#users-crud').append(user);
                    } else {
                        $("#user_id_" + data[2]).replaceWith(user).fadeIn('slow');
                    }
                    $('#userForm').trigger("reset");
                    $('#ajax-crud-modal').modal('hide');
                    $('#btn-save').html('Save Changes');
                    debugger
                    $("#errors").html(" ");
                }
                else
                {
                    $('#errors').html(data[1]);
                    //$('#userForm').trigger("reset");

                    $('#btn-save').html('Save Changes');

                }


            },
            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Save Changes');
            }
        });
      });
      /*
       <label class="col-sm-3 control-label">Mobile 1</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="mob1" name="mob1" placeholder="Enter Mobile" value="" required="">

                    </div>
        */
        var mobsdata=[];
        $('body').on('click', '#btn-add-mob', function () {
            mobcount++;
            var s='<div class="form-inline"><label class="col-sm-3 control-label">Mobile '+mobcount+'</label>';
            s+='<input type="text" class="form-control col-sm-5" id="mob'+mobcount+'" name="mob'+mobcount+'" placeholder="Enter Mobile" value="" required="">';
            s+='  <a onclick="delmob('+mobcount+')" id="btn-del-mob"  class="btn btn-danger col-sm-4">Delete Mobile</a></div>';
            $("#mobs").append(s);
            $("#mcount").val(mobcount);
        });
        function delmob(mid)
        {
            debugger
             mobsdata=[];
             mobcount=$("#mcount").val();

            for (let index = 1; index <= mobcount; index++) {
                if(index!=mid)
                    mobsdata.push($("#mob"+index).val());

            }
            var aa="";
            mobcount--;
            for (let index =1; index <= mobsdata.length; index++) {

                 aa+='<div class="form-inline"><label class="col-sm-3 control-label">Mobile '+index+'</label>';
            aa+='<input type="text" class="form-control col-sm-5" id="mob'+index+'" name="mob'+index+'" placeholder="Enter Mobile" value="'+mobsdata[index-1]+'" required="">';
            aa+='  <a onclick="delmob('+index+')" id="btn-del-mob"  class="btn btn-danger col-sm-4">Delete Mobile</a></div>';
            }
            $("#mcount").val(mobcount);
            $("#mobs").html(aa);
        }

  </script>
</html>
