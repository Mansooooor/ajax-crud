<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>laravel 6 First Ajax CRUD Application - Tutsmake.com</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

 <style>
   .container{
    padding: 0.5%;
   }
</style>
</head>
<body>

<div class="container">
        <form  method="POST" action="/ajax-crud/store" >
            @csrf

                <input type="hidden" name="user_id" id="user_id">
                 <div class="form-group">
                     <label for="name" class="col-sm-2 control-label">Name</label>
                     <div class="col-sm-12">
                         <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" >
                     </div>
                 </div>

                 <div class="form-group">
                     <label class="col-sm-2 control-label">Email</label>
                     <div class="col-sm-12">
                         <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="" >
                     </div>
                 </div>
                 <label class="col-sm-3 control-label">Mobile 1</label>
                 <div class="col-sm-12">
                     <input type="text" class="form-control" id="mob1" name="mob1" placeholder="Enter Mobile" value="" >

                 </div>
                 <label class="col-sm-3 control-label">Mobile 1</label>
                 <div class="col-sm-12">
                     <input type="text" class="form-control" id="mob2" name="mob2" placeholder="Enter Mobile" value="" >

                 </div>
                 <input type="hidden" name="mcount" id="mcount" value="2">
                 <input type="submit" class="btn btn-primary">
             </form>
  </div>
  <table class="table table-bordered" id="laravel_crud">
    <thead>
       <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
          <th>Mobile</th>
          <th colspan="2">Action</th>

       </tr>
    </thead>
    <tbody id="users-crud">
                     <tr id="user_id_1">
          <td>1</td>
          <td>Arabic</td>
          <td>cokuneva@example.net</td>

          <td><a href="javascript:void(0)" id="edit-user" data-id="1" class="btn btn-info">Edit</a></td>
          <td>
           <a href="javascript:void(0)" id="delete-user" data-id="1" class="btn btn-danger delete-user">Delete</a></td>
       </tr>
                     <tr id="user_id_3">
          <td>3</td>
          <td>shaban</td>
          <td>xfeeney@example.com</td>
          <td><a href="javascript:void(0)" id="edit-user" data-id="3" class="btn btn-info">Edit</a></td>
          <td>
           <a href="javascript:void(0)" id="delete-user" data-id="3" class="btn btn-danger delete-user">Delete</a></td>
       </tr>
                     <tr id="user_id_5">
          <td>5</td>
          <td>Frenchs</td>
          <td>allans36@example.org</td>
          <td><a href="javascript:void(0)" id="edit-user" data-id="5" class="btn btn-info">Edit</a></td>
          <td>
           <a href="javascript:void(0)" id="delete-user" data-id="5" class="btn btn-danger delete-user">Delete</a></td>
       </tr>
                     <tr id="user_id_6">
          <td>6</td>
          <td>ArabicCCddaa</td>
          <td>xaafsseeney@example.com</td>
          <td><a href="javascript:void(0)" id="edit-user" data-id="6" class="btn btn-info">Edit</a></td>
          <td>
           <a href="javascript:void(0)" id="delete-user" data-id="6" class="btn btn-danger delete-user">Delete</a></td>
       </tr>
                     <tr id="user_id_7">
          <td>7</td>
          <td>Arabicffff</td>
          <td>xfeffffeney@example.com</td>
          <td><a href="javascript:void(0)" id="edit-user" data-id="7" class="btn btn-info">Edit</a></td>
          <td>
           <a href="javascript:void(0)" id="delete-user" data-id="7" class="btn btn-danger delete-user">Delete</a></td>
       </tr>
                     <tr id="user_id_8">
          <td>8</td>
          <td>ahmed</td>
          <td>man@gma.com</td>
          <td><a href="javascript:void(0)" id="edit-user" data-id="8" class="btn btn-info">Edit</a></td>
          <td>
           <a href="javascript:void(0)" id="delete-user" data-id="8" class="btn btn-danger delete-user">Delete</a></td>
       </tr>

                  </tbody>
   </table>
</body>
</html>
