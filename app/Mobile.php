<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    //
    protected $table="mobile";
    protected $primayKey="id";
    protected $fillable = [
        'user_id', 'mobile',
    ];
    public function user()
    {
        return $this->belongsTo('App/User');
    }
}
