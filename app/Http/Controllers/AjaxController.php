<?php

namespace App\Http\Controllers;

use App\User;
use App\Mobile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect,Response;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::All();
        return view('ajax-crud',['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       $userId=0;
        $userId = $request->user_id;
       $validatearray=[];
        for ($i=1; $i <= $request->mcount; $i++) {
            $validatearray["mob$i"]="required|unique:mobile,mobile,$userId,user_id|regex:^(01)[0-9]{9}^";
            //
        }

        if($userId==0)
             {
                $validatearray['name']="required|unique:users,name|max:255";
               $validatearray['email'] = "required|email|unique:users,email";

                 $validator = Validator::make($request->all(),$validatearray);
        }
        else
             {
               $validatearray['name']="required|unique:users,name,$userId|max:255";
               $validatearray['email'] = "required|email|unique:users,email,$userId";

                 $validator = Validator::make($request->all(), $validatearray);
        }


        if ($validator->fails()) {
            $err="<div class='alert alert-danger'><strong>Errors</strong><ul>";
            $errors = $validator->errors();

            foreach($errors->all() as $error)
            {
                $err.="<li>".$error."</li>";
            }
            $err.="</ul></div>";

            $res=['fail',$err,0];
           return Response::json($res);

        }
        else {
        $userId = $request->user_id;
        $user   =   User::updateOrCreate(['id' => $userId],
                    ['name' => $request->name, 'email' => $request->email]);

        Mobile::where('user_id',$userId)->delete();

        $user1 = '<tr id="user_id_' . $user->id . '"><td>' . $user->id . '</td><td>' . $request->name . '</td><td>' . $request->email . '</td><td>';

                    for ($i=1; $i <= $request->mcount; $i++) {
             $mobile= new Mobile;
             $mobile->user_id=$user->id ;
             $mobile->mobile=$request->input("mob$i");
             $mobile->save();
             $user1.=$mobile->mobile."<br>";
        }

        $user1 .= '</td><td><a href="javascript:void(0)" id="edit-user" data-id="' . $user->id . '" class="btn btn-info">Edit</a></td>';
                    $user1 .= '<td><a href="javascript:void(0)" id="delete-user" data-id="' . $user->id . '" class="btn btn-danger delete-user">Delete</a></td></tr>';



        $res=['suc',$user1,$userId];
        return Response::json($res);
        }



         //var_dump($res);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $where1 = array('user_id' => $id);
        $user  = User::where($where)->first();
        $mobiles=Mobile::where($where1)->get();
        return Response::json([$user,$mobiles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id',$id)->delete();

        return Response::json($user);
        //
    }
}
